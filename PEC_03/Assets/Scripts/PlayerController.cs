using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private float moveHorizontal;
    private float moveVertical;
    public float playerSpeed;

    private bool haveAxe;
    private bool havePala;

    private GameObject uiManager;

    private void Awake()
    {
        uiManager = (GameObject)GameObject.FindGameObjectWithTag("UIManager");
    }

    // Start is called before the first frame update
    void Start()
    {
        haveAxe = false;
        havePala = false;
    }
    

    // Update is called once per frame
    void Update()
    {
        moveHorizontal = Input.GetAxis("Horizontal") * playerSpeed * Time.deltaTime;
        moveVertical = Input.GetAxis("Vertical") * playerSpeed * Time.deltaTime;
        this.gameObject.transform.Translate(moveHorizontal, moveVertical, 0.0f);
    }

    

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Axe")
        {
            Destroy(other.gameObject);
            haveAxe = true;
        }

        if (other.gameObject.tag == "Wood")
        {
           if (haveAxe == true)
            {
                Destroy(other.gameObject);
            }
        }

        if (other.gameObject.tag == "Pala")
        {
            Destroy(other.gameObject);
            havePala = true;
        }

        if (other.gameObject.tag == "Brick")
        {
            if (havePala == true)
            {
                other.gameObject.GetComponent<Rigidbody2D>().mass = 1;
                gameObject.GetComponent<Rigidbody2D>().mass = 1;
            }
        }
    }
    



}
