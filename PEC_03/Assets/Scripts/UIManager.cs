using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    private GameObject panelPause;
    private GameObject panelWin;


    private int statePause;
    private int statePanelPause;

    private void Awake()
    {
        panelPause = (GameObject)GameObject.FindGameObjectWithTag("PanelPause");
        panelWin = (GameObject)GameObject.FindGameObjectWithTag("PanelWin");
       


        statePanelPause = 0;
        Time.timeScale = 1.0f;

        NoShowPanel();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void NoShowPanel()
    {
        if (panelPause != null)
        {
            panelPause.SetActive(false);
        }
        if (panelWin != null)
        {
            panelWin.SetActive(false);
        }
      
    }

    public void ShowPanelPause(int state)
    {
        if (!panelPause.activeSelf)
        {
            panelPause.SetActive(true);
        }
        if (state == 1)
        {
            Debug.Log("Esta en Pausa");
        }
        else if (state == 2)
        {
            Debug.Log("Esta en Pausa 2");
        }
        else
        {
            Time.timeScale = 1.0f;
            Debug.Log("Esta pausa 3");
        }
    }

    public void ShowPanelWin()
    {
        if (panelWin)
        {
            panelWin.SetActive(true);
            Time.timeScale = 0.0f;
        }
    }

   


    //Main Menu
    public void Play()
    {
        SceneManager.LoadScene("Level_1");
    }

    public void Editor()
    {
        SceneManager.LoadScene("Level_0");
    }

    public void Credits()
    {
        SceneManager.LoadScene("Credits");
    }

    public void Exit()
    {
        Application.Quit();
    }

    public void Next2()
    {
        SceneManager.LoadScene("Level_2");
    }

    public void Next3()
    {
        SceneManager.LoadScene("Level_3");
    }

    public void Next4()
    {
        SceneManager.LoadScene("Level_4");
    }

    public void Next5()
    {
        SceneManager.LoadScene("Level_5");
    }

    public void Next6()
    {
        SceneManager.LoadScene("Level_6");
    }

    public void Next7()
    {
        SceneManager.LoadScene("Level_7");
    }

    public void Next8()
    {
        SceneManager.LoadScene("Level_8");
    }

    public void Next9()
    {
        SceneManager.LoadScene("Level_9");
    }

    public void Next10()
    {
        SceneManager.LoadScene("Level_10");
    }

    //Panel Over

    public void Again()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void MainMenu()
    {
        SceneManager.LoadScene("Menu");
    }

    //Panel Pause
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (statePanelPause == 0)
            {
                ShowPanelPause(statePanelPause);
                statePanelPause = 1;
                Time.timeScale = 0.0f;
            }
            else
            {
                NoShowPanel();
                statePanelPause = 0;
                Time.timeScale = 1.0f;
            }


        }
    }

}
