using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boxes : MonoBehaviour
{
    private GameObject uiManager;
    private GameObject player;
    private GameObject endPoint;   

    // Start is called before the first frame update
    private void Awake()
    {
        uiManager = (GameObject)GameObject.FindGameObjectWithTag("UIManager");
        player = (GameObject)GameObject.FindGameObjectWithTag("Player");
        endPoint = (GameObject)GameObject.FindGameObjectWithTag("Finish");
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void IsWinNow()
    {
        uiManager.GetComponent<UIManager>().ShowPanelWin();
    }
    


    private void OnTriggerEnter2D(Collider2D other)
    {
        
        if (other.gameObject.tag == "Finish")
        {
            IsWinNow();
        }


    }
}
